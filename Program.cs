﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Desafio_do_Xadrez
{
    class Program
    {
        
        static void Main(string[] args)
        {
            int[,] tabuleiro = new int[,]
            {
                {4,3,2,5,6,2,3,4},
                {1,1,1,1,1,1,1,1},
                {0,0,0,0,0,0,0,0},
                {0,0,0,0,0,0,0,0},
                {0,0,0,0,0,0,0,0},
                {0,0,0,0,0,0,0,0},
                {1,1,1,1,1,1,1,1},
                {4,3,2,5,6,2,3,4},
            };

            int numpeao = 0, numbispo = 0, numcavalo = 0, numtorre = 0, numrainha = 0, numrei = 0;

            for (int i = 0; i <= tabuleiro.GetLength(0) - 1; i++)
            {
                for (int j = 0; j <= tabuleiro.GetLength(1) - 1; j++)
                {
                 while(tabuleiro[i,j] == 1)
                    {
                        numpeao += 1;
                        break;
                    }

                while (tabuleiro[i, j] == 2)
                    {
                        numbispo += 1;
                        break;
                    }

                while (tabuleiro[i, j] == 3)
                    {
                        numcavalo += 1;
                        break;
                    }

                while (tabuleiro[i, j] == 4)
                    {
                        numtorre += 1;
                        break;
                    }

                while (tabuleiro[i, j] == 5)
                    {
                        numrei += 1;
                        break;
                    }

                while (tabuleiro[i, j] == 6)
                    {
                        numrainha += 1;
                        break;
                    }
                }
            }

            Console.WriteLine("Peao: " + numpeao + " Pecas");
            Console.WriteLine("Bispo: " + numbispo + " Pecas");
            Console.WriteLine("Cavalo: " + numcavalo + " Pecas");
            Console.WriteLine("Torre: " + numtorre + " Pecas");
            Console.WriteLine("Rainha: " + numrainha + " Pecas");
            Console.WriteLine("Rei: " + numrei + " Pecas");
            Console.ReadLine();
        }
    }
}
